<!DOCTYPE html>
<html>
<body>

<h1>Buat Akun Baru!</h1>
<h3>Sign Up Form</h3>

<form action="/submit" method="POST">
    @csrf
    <label for="fname">First name:</label><br><br>
    <input type="text" id="fname" name="fname"><br><br>
    <label for="lname">Last name:</label><br><br>
    <input type="text" id="lname" name="lname"><br><br>
    <label for="gender">Gender:</label><br><br>
        <label><input type="radio" name="gender" value="Male"> Male</label><br>
        <label><input type="radio" name="gender" value="Female"> Female</label><br>
        <label><input type="radio" name="gender" value="Other"> Other</label><br>
    <br>
    <label for="nationality">Nationality:</label><br><br>
    <select name="nationality">
        <option>Indonesian</option>
        <option>French</option>
        <option>English</option>
        <option>German</option>
        <option>Italian</option>
    </select>
    <br><br>
    <label for="language-spoken">Language Spoken:</label><br><br>
        <label><input type="radio" name="language-spoken" value="Bahasa Indonesia"> Bahasa Indonesia</label><br>
        <label><input type="radio" name="language-spoken" value="English"> English</label><br>
        <label><input type="radio" name="language-spoken" value="Other"> Other</label><br>
    <br><br>
    <label for="bio">Bio: </label><br>
    <textarea name="bio"></textarea>
    <br><br><br>
    <input type="submit" value="Submit">
</form> 

</body>
</html>